#iRule-LOG4J-Mitigation [13-Dec-2021]

when HTTP_REQUEST {

 # Retrieve "real" client IP
 if { [HTTP::header exists X-Forwarded-IP] } {
  set DEF_CLIENT_IP [HTTP::header value "X-Forwarded-IP"]
  set DEF_CDN "CDNetworks"
 } elseif { [HTTP::header exists Cdn-Src-Ip] } {
  set DEF_CLIENT_IP [HTTP::header value "Cdn-Src-Ip"]
  set DEF_CDN "CDNetworks-CNC"
 } elseif { [HTTP::header exists True-Client-IP] } {
  set DEF_CLIENT_IP [HTTP::header value "True-Client-IP"]
  set DEF_CDN "Akamai"
 } elseif { [HTTP::header exists Edgecast-Client-IP] } {
  set DEF_CLIENT_IP [HTTP::header value "Edgecast-Client-IP"]
  set DEF_CDN "EdgeCast"
 } elseif { [HTTP::header exists Incap-Client-IP] } {
  set DEF_CLIENT_IP [HTTP::header value "Incap-Client-IP"]
  set DEF_CDN "Incapsula"
 } else {
  set DEF_CLIENT_IP [IP::client_addr]
  set DEF_CDN "No-CDN"
 }

 # Retrieve HTTP_HOST in lowercase
 set DEF_HTTP_HOST [string tolower [HTTP::host]]

  # Version 2.0 - 2021-12-11 23:40 Eastern
  # - Handling nested URI encoding
  # - Improved matching
  # Version 1.0 - 2021-12-11 06:10 Eastern
  # - Initial release
  # less aggressive regexp for those concerned about false positives "\$\{(\$\{env:[^:]+:-|\$\{[a-z]+:)?j\}?(\$\{env:[^:]+:-|\$\{[a-z]+:)?n.+:.+\}" (remove quotes)
  # very aggressive regexp "\$\{.+?\}" (remove quotes)
  # URI – based on 200004474
  set tmpUri [HTTP::uri -normalized]
  set uri [URI::decode $tmpUri]
  while { $uri ne $tmpUri } {
    set tmpUri $uri
    set uri [URI::decode $tmpUri]
  }

  if {[string tolower $uri] matches_regex {\$\{}} {
    log local0. "log4j_rce_detection drop from CLIENT-IP: $DEF_CLIENT_IP - CDN: $DEF_CDN - Hostname: $DEF_HTTP_HOST on URI: $uri"
    drop
    event disable all
    return
  }

  set tmpReq [HTTP::request]
  set req [URI::decode $tmpReq]
  while { $req ne $tmpReq } {
    set tmpReq $req
    set req [URI::decode $tmpReq]
  }

  # Header – looks for ${j…} or ${${…}}
  if {[string tolower $req] matches_regex {\$\{\s*(j|\$\{).+?\}}} {
    log local0. "log4j_rce_detection drop from CLIENT-IP: $DEF_CLIENT_IP - CDN: $DEF_CDN - Hostname: $DEF_HTTP_HOST on header: $req"
    drop
    event disable all
    return
  }

  # Payload – looks for ${j…} or ${${…}}
  if {[HTTP::method] eq "POST"}{
    # Trigger collection for up to 1MB of data
    if {[HTTP::header "Content-Length"] ne "" && [HTTP::header "Content-Length"] <= 1048576}{
      set content_length [HTTP::header "Content-Length"]
    } else {
      set content_length 1048576
    }
    # Check if $content_length is not set to 0
    if { $content_length > 0} {
      HTTP::collect $content_length
    }
  }
}
when HTTP_REQUEST_DATA {
 # Retrieve "real" client IP
 if { [HTTP::header exists X-Forwarded-IP] } {
  set DEF_CLIENT_IP [HTTP::header value "X-Forwarded-IP"]
  set DEF_CDN "CDNetworks"
 } elseif { [HTTP::header exists Cdn-Src-Ip] } {
  set DEF_CLIENT_IP [HTTP::header value "Cdn-Src-Ip"]
  set DEF_CDN "CDNetworks-CNC"
 } elseif { [HTTP::header exists True-Client-IP] } {
  set DEF_CLIENT_IP [HTTP::header value "True-Client-IP"]
  set DEF_CDN "Akamai"
 } elseif { [HTTP::header exists Edgecast-Client-IP] } {
  set DEF_CLIENT_IP [HTTP::header value "Edgecast-Client-IP"]
  set DEF_CDN "EdgeCast"
 } elseif { [HTTP::header exists Incap-Client-IP] } {
  set DEF_CLIENT_IP [HTTP::header value "Incap-Client-IP"]
  set DEF_CDN "Incapsula"
 } else {
  set DEF_CLIENT_IP [IP::client_addr]
  set DEF_CDN "No-CDN"
 }

 # Retrieve HTTP_HOST in lowercase
 set DEF_HTTP_HOST [string tolower [HTTP::host]]

  set tmpPayload [HTTP::payload]
  set payload [URI::decode $tmpPayload]
  while { $payload ne $tmpPayload } {
    set tmpPayload $payload
    set payload [URI::decode $tmpPayload]
  }

  if {[string tolower $payload] matches_regex {\$\{\s*(j|\$\{).+?\}}} {
    log local0. "log4j_rce_detection drop from CLIENT-IP: $DEF_CLIENT_IP - CDN: $DEF_CDN - Hostname: $DEF_HTTP_HOST on payload"
    drop
    event disable all
  }
}